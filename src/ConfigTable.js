import React from 'react';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import DeleteIcon from 'material-ui/svg-icons/action/delete';
import ConfigIcon from 'material-ui/svg-icons/action/build';
import AddIcon from 'material-ui/svg-icons/content/add';
import IconButton from 'material-ui/IconButton';
import './ConfigTable.css';

export default class ConfigTable extends React.Component {

    constructor(props) {
        super(props);
        this.index = [];
        const headers = [];
        let key;
        this.props.headers.forEach((item) => {
            if (item.title) {
                this.index.push(item.index); 
                headers.push(<TableHeaderColumn>{item.title}</TableHeaderColumn>);
                if (item.isKey) {
                    key = item.index;
                }
            }
        });
        this.state = {
            data: this.props.data,
            'headers': headers,
            'key': key,
        };
    }

    buildTableBody() {
        const rows = this.state.data.map((field) =>{
            const items = this.index.map((index) => {
                return(
                    <TableRowColumn>{field[index]}</TableRowColumn>
                );
            });
            return (
                <TableRow key={field[this.state.key]}>
                    {items}
                </TableRow>
            )
        });

        return rows;
    }

    clickWrapper(event) {
        const id = (this.selectedRow!==undefined) ? this.state.data[this.selectedRow][this.state.key] : undefined;
        switch(event) {
            case 'delete':
                this.props.onDeleteClick(id);
                break;
            case 'edit':
                this.props.onEditClick(id);
                break;
            case 'add':
                this.props.onAddClick(id);
                break;
        }
    }

    onRowSelection(rows) {
        this.selectedRow = rows[0];
        console.log(this.selectedRow);
    }
    
    render() {
        
        
        return (
            <div>
                <div className='control-panel'>
                    <div>
                        <MuiThemeProvider>
                            <IconButton onClick={this.clickWrapper.bind(this, 'add')}>
                                <AddIcon/>
                            </IconButton>
                        </MuiThemeProvider>
                    </div>
                    <div>
                        <MuiThemeProvider>
                            <IconButton onClick={this.clickWrapper.bind(this, 'edit')}>
                                <ConfigIcon/>
                            </IconButton>
                        </MuiThemeProvider>
                    </div>

                    <div>
                        <MuiThemeProvider>
                            <IconButton onClick={this.clickWrapper.bind(this, 'delete')}>
                                <DeleteIcon/>
                            </IconButton>
                        </MuiThemeProvider>
                    </div>
                </div>
                <div className='table-panel'>
                    <MuiThemeProvider>
                        <Table onRowSelection={this.onRowSelection.bind(this)}>
                        <TableHeader>
                        <TableRow>
                            {this.state.headers}
                        </TableRow>
                        </TableHeader>
                        <TableBody deselectOnClickaway={false} showRowHover={true}>
                            {this.buildTableBody()}
                        </TableBody>
                        </Table>
                    </MuiThemeProvider>
                </div>
            </div>
        )
    }
}