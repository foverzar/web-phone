import React from "react";
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import FlatButton from 'material-ui/FlatButton';
import IconButton from 'material-ui/IconButton';
import SettingsIcon from 'material-ui/svg-icons/action/settings';
import PlayIcon from 'material-ui/svg-icons/av/play-arrow';
import PauseIcon from 'material-ui/svg-icons/av/pause';
import ContactPhoneIcon from 'material-ui/svg-icons/communication/contact-phone';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';
import {NavLink} from 'react-router-dom';

import './Panel.css'

function BreakButton(props) {

    const switchTo = !props.pushed;
    const Icon = props.pushed ? PlayIcon : PauseIcon;

    return (
        <IconButton onClick={ props.handler.bind(null, switchTo) }>
            <Icon/>
        </IconButton>
    )
}

export default class Panel extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        

        return (
            <div>
            <MuiThemeProvider>
                <Toolbar>
                    <ToolbarGroup firstChild={true}>
                        <NavLink
                            exact
                            to='/'
                        >
                            <IconButton>
                                <ContactPhoneIcon />
                            </IconButton>
                        </NavLink>
                        <NavLink
                            to="/config"
                        >
                            <IconButton>
                                <SettingsIcon />
                            </IconButton>
                        </NavLink>
                    </ToolbarGroup>
                    <ToolbarGroup>
                        <BreakButton pushed={this.props.isOnBreak} handler = { this.props.breakHandler }/>
                    </ToolbarGroup>
                    <ToolbarGroup>
                        <FlatButton label="Выход" href="/user/logout"/>
                    </ToolbarGroup>
                </Toolbar>
            </MuiThemeProvider>
            </div>
        );
    }
}
