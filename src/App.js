import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import CTI from './CTI.js';
import Panel from './Panel.js';
import ScenarioView from './ScenarioView';
import ConfigView from './ConfigView';
import CampaignEditView from './CampaignEditView'
import ClientEditView from './ClientEditView'
import update from 'immutability-helper';
import {
    HashRouter as Router,
    Route,
    Link,
    Switch
} from 'react-router-dom';


class App extends Component {
    constructor(props) {
        super(props)
        this.state = ({
            src: '64x64',
            isOnBreak: true,
            callStatus: { status: 'none' },
        })
        this.isIterating = false
        this.isReadyForRequest = true
        this.buildConfig()
        this.cti = undefined
        this.timerID = undefined
    }

    componentWillUpdate(nextProps, nextState) {
        console.log('componentWillUpdate')
        if (this.state.isOnBreak && !nextState.isOnBreak) {
            setTimeout(this.workNextContact.bind(this), 100)
            this.timerID = setInterval(this.workNextContact.bind(this), 1500)
            console.log('Timer set to '+this.timerID)
        } else if (!this.state.isOnBreak && nextState.isOnBreak) {
            console.log('Clearing interval')
            clearInterval(this.timerID)
        }
    }

    buildConfig() {
        fetch('/api/users/current', {
            method: 'GET',
            credentials: 'include',
        }).then((res) => {
            res.json().then((data) => {
                const config = {}
                config['uri'] = data['sip_uri']
                config['wsServers'] = [data['sip_server']]
                config['authorizationUser'] = data['sip_username']
                config['password'] = data['sip_password']
                config['iceCheckingTimeout'] = 500
                config['traceSip'] = true

                this.setState({'config': config})
            })
        })
    }

    breakHandler(val) {
        this.setState({ isOnBreak: val });
    }

    workNextContact() {
        console.log('workNextContact() heartbeat. Will fetch? '+ this.isReadyForRequest)
        if (this.isReadyForRequest) {
            this.isReadyForRequest = false
            console.log('workNextContact() sending request')
            fetch('/api/taskqueue/lease', {
                method: 'POST',
                credentials: 'include',
            }).then((res) => {
                console.log('Done fetching data')
                if (res.status === 204) {
                    console.log('No tasks in queue')
                    this.isReadyForRequest = true
                    return
                }
                res.json().then((data) => {
                    console.log('Parsed data')
                    this.setState({ callStatus: { campaignId: data.campaignId, clientId: data.clientId , status: 'ongoing' } })
                    this.cti.call(data.contact)
                    this.registerSessionEventHandlers()
                })
            })
        }
    }

    registerSessionEventHandlers() {
        this.cti.session.on('terminated', () => {
            console.log('Terminated, changing call status to report mode')
            this.setState((prevState) => {
                return update(this.state, {callStatus: {status: {$set: 'report'}}})
            })
        }) 
    }

    doneContact() {
        this.setState({ callStatus: {status: 'none'} })
        this.isReadyForRequest = true
    }

  render() {
    return (
        <Router>
          <div className="App">
            <Panel isOnBreak = {this.state.isOnBreak} breakHandler = { this.breakHandler.bind(this) }/>
            <Route exact path="/" component={() => ( <ScenarioView callStatus = {this.state.callStatus} doneContact = {this.doneContact.bind(this)} />) } /> { /* FIXME Always re-renders */ }
            <Route exact path="/config" component={ConfigView} />
            <Route exact path="/config/edit/:id" component={CampaignEditView} />
            <Route exact path="/config/edit" component={CampaignEditView} />
            <Route exact path="/config/edit/:campaignId/client/:clientId" component={ClientEditView} />
            <Route exact path='/config/edit/:campaignId/client' component={ClientEditView} />
            <CTI config={this.state.config} ref = { cti => this.cti = cti }/>
          </div>
        </Router>
    );
  }
}

export default App;
