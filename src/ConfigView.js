import React from 'react';
import ConfigTable from './ConfigTable';
import CircularProgress from 'material-ui/CircularProgress';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import EnabledIcon from 'material-ui/svg-icons/navigation/check';
import {Route} from 'react-router-dom';

const headers = [
    {index: 'name', title: 'Название'},
    {index: 'isEnabled', title: 'Включена'},
    {index: '_id', title: 'ID', isKey: true },
]

const style = {
    margin: '0 auto',
    display: 'block',
    'padding-top': '10%',
};

export default class ConfigView extends React.Component {

    data = []

    constructor(props) {
        super(props);
        this.state = {
            redirect: undefined,
            isFetched: false,
        }
        this.fetchData();
    }

    fetchData() {
        fetch('/api/campaigns', {
            method: 'GET',
            credentials: 'include',
        }).then((res) => {
            res.json().then((data)=>{
                this.data = data.map((item) => {
                    item.isEnabled = item.isEnabled ? <EnabledIcon/> : null;
                    console.log(item);
                    return item;
                });
                this.setState({isFetched: true});
            });
            
        });
    }

    onDeleteClick(id) {
        if (id!==undefined) {
            this.setState({isFetched: false});
            fetch('/api/campaigns/' + id, {
                method: 'DELETE',
                credentials: 'include',
            }).then((res) => {
                this.fetchData();
            });
        }
    }

    onAddClick(id) {
        this.redirect('/config/edit');
    }

    
    onEditClick(id) {
        if (id!==undefined) this.redirect('/config/edit/' + id);
    }

    redirect(path) {
        this.props.history.push(path);
    }
    
    render() {

        return (
            <div className='view'>
                    
                    {     this.state.isFetched ?
                            <ConfigTable onDeleteClick={this.onDeleteClick.bind(this)} onEditClick={this.onEditClick.bind(this)} onAddClick={this.onAddClick.bind(this)} headers={headers} data={this.data||[]}/>
                            : (
                                <MuiThemeProvider>
                                    <CircularProgress size={100} thickness={10} style={style}/>
                                </MuiThemeProvider>)
                    }   
                    
            </div>
        )
    }
}
