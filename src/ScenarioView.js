import React from 'react'
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton'
import MuiThemeProvider from '../node_modules/material-ui/styles/MuiThemeProvider'
import RaisedButton from 'material-ui/RaisedButton'
import './ScenarioView.css'

const style = {
    marginBottom: 16,
}

export default class ScenarioView extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            statusValue: 'done',
            buttonDisabled: false,
        }
    }

    sendReport() {
        this.setState({buttonDisabled: true})
        fetch('/api/taskqueue/report?campaignId='+this.props.callStatus.campaignId+'&clientId='+this.props.callStatus.clientId, {
            method: 'POST',
            credentials: 'include',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({status: this.state.statusValue}),
        }).then(this.props.doneContact)
    }

    handleChange(e, val) {
        this.setState({ statusValue: val })
    }

    render() {
        switch(this.props.callStatus.status) {
            case 'ongoing':
                return <iframe className="view" src={"/scenario?campaignId="+this.props.callStatus.campaignId+"&clientId="+this.props.callStatus.clientId} />
            case 'report':
                return (
                    <div className="view">
                        <div className="report-form">
                        <MuiThemeProvider>
                             <RadioButtonGroup name="shipSpeed" onChange={this.handleChange.bind(this)} valueSelected={this.state.statusValue}>
                                <RadioButton
                                    style={style}
                                    value="done"
                                    label="Диалог с клиентом завершен успешно"
                                />
                                <RadioButton
                                    style={style}
                                    value="redial"
                                    label="Необходимо перезвонить"
                                />
                            </RadioButtonGroup>
                        </MuiThemeProvider>
                        <MuiThemeProvider>
                            <RaisedButton label="Сохранить" primary={true} disabled={this.state.buttonDisabled} onClick={this.sendReport.bind(this)} />
                        </MuiThemeProvider>
                        </div>
                    </div>
                )
            default:
                console.warn('No call status set') // No break here, none is default fallback
            case 'none': 
                return ( <div className="view"> Нет активного задания </div> )
        }
    }
}
