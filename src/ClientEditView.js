import React from 'react';
import TextField from 'material-ui/TextField';
import Toggle from 'material-ui/Toggle';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import ConfigTable from './ConfigTable';
import CircularProgress from 'material-ui/CircularProgress';
import Paper from 'material-ui/Paper';
import update from 'immutability-helper';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import './form.css'


const style = {
    margin: '0 auto',
    display: 'block',
    'padding-top': '10%',
};

export default class ClientEditView extends React.Component {

    constructor(props) {
        super(props)
        console.log(this.props);
        this.state = {
            isFetched: this.props.match.params.clientId ? false : true,
            item: { displayname: "", contact: "", data: "" },
        }
        if (this.props.match.params.clientId) {
            this.fetchData();
        }
    }

    fetchData() {
        fetch('/api/campaigns/' + this.props.match.params.campaignId + '/clients/' + this.props.match.params.clientId,  {
            method: 'GET',
            credentials: 'include',
        }).then((res) => {
            res.json().then((data)=>{
                const item = {}
                item['displayname'] = data['displayname']
                item['contact'] = data['contact']
                item['data'] = {}
                for (let index in data) {
                    if (index !== 'contact' && index !== 'displayname' && index !=='_id' && index !== 'contact_status_code') {
                        item['data'][index] = data[index]
                    }
                }
                item.data = JSON.stringify(item.data, null, 2)
                this.setState({isFetched: true, 'item': item});
            });
            
        });
    }

    saveData() {
        let fetchPromise
        const item = {}

        for (let index in this.state.item) {
            if (index !== '_id' && index !== 'data') {
                item[index] = this.state.item[index]
            }
        }

        const data = JSON.parse(this.state.item.data)

        for (let index in data) {
            item[index] = data[index]
        }

        console.log(item);
        
        if (this.props.match.params.clientId) {
            fetchPromise = fetch('/api/campaigns/' + this.props.match.params.campaignId + '/clients/' + this.props.match.params.clientId, {
                method: 'PATCH',
                credentials: 'include',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(item),
            })
        } else {
            fetchPromise = fetch('/api/campaigns/' + this.props.match.params.campaignId + '/clients', {
                method: 'POST',
                credentials: 'include',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(item),
            })
        }
        
        fetchPromise.then((res) => {
            this.props.history.push('/config/edit/' + this.props.match.params.campaignId);
        })
    }

    nameHandler(e, value) {
        this.setState((prevState) => {
            return update(this.state, {item: {displayname: {$set: value}}})
        })
    }

    contactHandler(e, value) {
        this.setState((prevState) => {
            return update(this.state, {item: {contact: {$set: value}}})
        })
    }

    dataHandler(e, value) {
        this.setState((prevState) => {
            return update(this.state, {item: {data: {$set: value}}})
        })
    }


    render() {
        return (
            <div className='view'>
            { this.state.isFetched
                ? (     
                        <div className="form">
                            <MuiThemeProvider>
                                <Paper>
                                    <div className="form-content">
                                        <TextField
                                            floatingLabelText="Имя (параметр displayname)"
                                            onChange={this.nameHandler.bind(this)}
                                            fullWidth={true}
                                            value={this.state.item.displayname}
                                        />
                                        <TextField
                                            floatingLabelText="Контактный номер (параметр contact)"
                                            onChange={this.contactHandler.bind(this)}
                                            fullWidth={true}
                                            value={this.state.item.contact}
                                        />
                                        <TextField
                                            floatingLabelText="Дополнительные данные"
                                            multiLine={true}
                                            onChange={this.dataHandler.bind(this)}
                                            value={this.state.item.data}
                                            fullWidth={true}
                                            rows={5}
                                        />
                                    </div>
                                    <div className="form-controls">
                                        <FlatButton label="Закрыть" />
                                        <RaisedButton label="Сохранить" primary={true} onClick={this.saveData.bind(this)}/>
                                    </div>
                                </Paper>
                            </MuiThemeProvider>
                        </div>
                    
                    
            ) : (
                    <MuiThemeProvider>
                        <CircularProgress size={100} thickness={10} style={style}/>
                    </MuiThemeProvider>
                )
            }
            </div>

                


            
        )
    }
}