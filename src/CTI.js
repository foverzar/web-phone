import React from 'react';

import './CTI.css';

import MuiThemeProvider from '../node_modules/material-ui/styles/MuiThemeProvider';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from '../node_modules/material-ui/Toolbar';
import FloatingActionButton from '../node_modules/material-ui/FloatingActionButton';
import CallAccept from '../node_modules/material-ui/svg-icons/communication/call';
import CallEnd from '../node_modules/material-ui/svg-icons/communication/call-end';
import DialPad from '../node_modules/material-ui/svg-icons/communication/dialpad';
import CallHold from '../node_modules/material-ui/svg-icons/notification/phone-paused';
import CallTransfer from '../node_modules/material-ui/svg-icons/notification/phone-forwarded';
import TextField from '../node_modules/material-ui/TextField';

import SIP from '../node_modules/sip.js/src/index.js'


const style = {
  marginRight: 20,
}

export default class CTI extends React.Component {
  constructor(props) {
    super(props)
    this._config = props.config
    if (this._config) {
      this.createUA();
      this.setupMediaStream();
    }

    this.state = {
      statusClass: 'status-none',
      isOnHold: false,
    }
  }

  componentWillReceiveProps(nextProps) {
    if (JSON.stringify(this._config) != JSON.stringify(nextProps.config)) {
      this._config = nextProps.config
      this.createUA();
      this.setupMediaStream();
    }
  }

  createUA() {
    console.log(this._config)
    this._userAgent = new SIP.UA(this._config);
    this.registerUaEventHandlers(); 
  }

  setupMediaStream(options) {
    let constraints = { audio: true, video: false };
    navigator.mediaDevices.getUserMedia(constraints).then((stream) => {
      this._stream = stream;
    }).catch((e) => {
        console.error('getUserMedia failed:', e);
    });
  }  


  registerUaEventHandlers() {
    this._userAgent.on('registered', () => {
      this.setStatus('connected');
    });
    this._userAgent.on('connecting', () => {
      this.setStatus('connecting');
    });
    
  }

  registerSessionEventHandlers() {
    this.session.on('hold', () => {
        this.setStatus('hold')
    })
    this.session.on('unhold', () => {
        this.setStatus('call')
    })
    this.session.on('terminated', () => {
        this.setStatus('connected')
    })
    this.session.on('accepted', () => {
        this.setStatus('call')
    })
    this.session.on('progress', () => {
        this.setStatus('progress')
    })
    
  }

  setStatus(status) {
    console.log('Setting status: ' + status)
    switch (status) {
      case 'connecting':
        this.setState({ statusClass: 'status-connecting', isOnHold: false })
        break
      case 'connected':
        this.setState({ statusClass: 'status-connected', isOnHold: false })
        break
      case 'hold':
        this.setState({ statusClass: 'status-hold', isOnHold: true })
        break
      case 'call':
        this.setState({ statusClass: 'status-call', isOnHold: false })
        break
      case 'progress':
        this.setState({ statusClass: 'status-progress', isOnHold: false })
        break
      default:
        console.error('Unknown status: ' + status)
        break
    }
  } 

  hold() {
    if (this.session) {
        if (!this.state.isOnHold) {
            this.session.hold();
        } else {
            this.session.unhold();
        }
    }
  }

  hang() {
    if (this.session) {
        this.session.cancel();
    }
  }

  transfer(target) {
    if (this.session) {
        this.session.refer( (target ? target : this.callTarget) );
    }
  }

  call(target) {
    console.log(target)
    this.session = this._userAgent.invite( (target ? target : this.callTarget), {
      media: {
        stream: this._stream,
//        audio: true,
//        video: false,
      },
      render: {
        remote: document.getElementById('audio')
      }
    });
    this.registerSessionEventHandlers()
    this.setStatus('progress')
  }

  callTargetInputHandler(e, value) {
    this.callTarget = value;
  }
  
  // shouldComponentUpdate(nextProps, nextState) {
  //   if (this.props == nextProps && this.state == nextState) {
  //     console.log('No update')
  //     return false
  //   } else {
  //     console.log('Update')
  //     return true
  //   }
  // }

  render() {
    return (
      <div>
        <MuiThemeProvider>
          <Toolbar >
            <ToolbarGroup>
                <ToolbarTitle text="Статус"/>
                <span className={this.state.statusClass} />
            </ToolbarGroup>
            <ToolbarGroup>
                <TextField
                    hintText="Вызываемый номер"
                    onChange={this.callTargetInputHandler.bind(this)} 
                    style={style}
                />
              <FloatingActionButton 
                backgroundColor="green"
                style={style}
                onClick={this.call.bind(this, undefined)}
                mini={true}
              >
                <CallAccept/>
              </FloatingActionButton>
              <FloatingActionButton
                backgroundColor="red"
                style={style}
                onClick={this.hang.bind(this)}
                mini={true}
              >
                <CallEnd/>
              </FloatingActionButton>
              <FloatingActionButton
                backgroundColor="#FFD300"
                style={style}
                mini={!this.state.isOnHold}
                onClick={this.hold.bind(this)}
              >
                <CallHold/>
              </FloatingActionButton>

              <FloatingActionButton
                backgroundColor="#FFA500"
                style={style}
                mini={true}
                onClick={this.transfer.bind(this)}
              >
                <CallTransfer/>
              </FloatingActionButton> 

            </ToolbarGroup>
          </Toolbar>
        </MuiThemeProvider>
        <audio id='audio' ref={(audio) => { this._audio = audio; }}/>
      </div>
    );
  }
}
