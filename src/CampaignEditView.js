import React from 'react';
import TextField from 'material-ui/TextField';
import Toggle from 'material-ui/Toggle';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import ConfigTable from './ConfigTable';
import CircularProgress from 'material-ui/CircularProgress';
import Paper from 'material-ui/Paper';
import update from 'immutability-helper';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import './form.css'


const headers = [
    {index: 'displayname', title: 'Имя'},
    {index: 'contact', title: 'Контактный телефон'},
    {index: '_id', title: 'ID', isKey: true },
]

const style = {
    margin: '0 auto',
    display: 'block',
    'padding-top': '10%',
};

export default class CampaignEditView extends React.Component {

    constructor(props) {
        super(props)
        console.log(this.props);
        this.state = {
            isFetched: this.props.match.params.id ? false : true,
            item: { name: "", isEnabled: false, template: "" },
        }
        if (this.props.match.params.id) {
            this.fetchData();
        }
    }

    fetchData(id) {
        fetch('/api/campaigns/' + this.props.match.params.id, {
            method: 'GET',
            credentials: 'include',
        }).then((res) => {
            res.json().then((data)=>{
                this.setState({isFetched: true, item: data});
            });
            
        });
    }

    saveData() {
        let fetchPromise
        const item = {}

        for (let index in this.state.item) {
            if (index !== '_id' && index !== 'clients') {
                item[index] = this.state.item[index];
            }
        }
        console.log(item);
        
        if (this.props.match.params.id) {
            fetchPromise = fetch('/api/campaigns/' + this.props.match.params.id, {
                method: 'PATCH',
                credentials: 'include',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(item),
            })
        } else {
            fetchPromise = fetch('/api/campaigns', {
                method: 'POST',
                credentials: 'include',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(item),
            })
        }
        
        fetchPromise.then((res) => {
            this.props.history.push('/config');
        })
    }

    nameHandler(e, value) {
        this.setState((prevState) => {
            return update(this.state, {item: {name: {$set: value}}})
        })
    }

    templateHandler(e, value) {
        this.setState((prevState) => {
            return update(this.state, {item: {template: {$set: value}}})
        })
    }

    enabledHandler(e, value) {
        this.setState((prevState) => {
            return update(this.state, {item: {isEnabled: {$set: value}}})
        })
    }

    onDeleteClick(id) {
        if (id!==undefined) {
            this.setState({isFetched: false});
            fetch('/api/campaigns/' + id, {
                method: 'DELETE',
                credentials: 'include',
            }).then((res) => {
                this.fetchData();
            });
        }
    }

    onAddClick(id) {
        if (this.state.item._id!==undefined) this.redirect('/config/edit/' + this.state.item._id + '/client')
    }

    
    onEditClick(id) {
        if (id!==undefined) this.redirect('/config/edit/' + this.state.item._id + '/client/' + id)
    }

    redirect(path) {
        this.props.history.push(path)
    }

    render() {
        return (
            <div className='view'>
            { this.state.isFetched
                ? (     <div>
                        <div className="form">
                            <MuiThemeProvider>
                                <Paper>
                                    <div className="form-content">
                                        <TextField
                                            floatingLabelText="Название"
                                            onChange={this.nameHandler.bind(this)}
                                            fullWidth={true}
                                            value={this.state.item.name}
                                        />
                                        <Toggle
                                            label="Включена"
                                            defaultToggled={this.state.item.isEnabled}
                                            labelPosition="right"
                                            onToggle={this.enabledHandler.bind(this)}
                                        />
                                        <TextField
                                            floatingLabelText="Шаблон"
                                            multiLine={true}
                                            onChange={this.templateHandler.bind(this)}
                                            value={this.state.item.template}
                                            fullWidth={true}
                                            rows={5}
                                        />
                                    </div>
                                    <div className="form-controls">
                                        <FlatButton label="Закрыть" />
                                        <RaisedButton label="Сохранить" primary={true} onClick={this.saveData.bind(this)}/>
                                    </div>
                                </Paper>
                            </MuiThemeProvider>
                        </div>
                        <ConfigTable onDeleteClick={this.onDeleteClick.bind(this)} onEditClick={this.onEditClick.bind(this)} onAddClick={this.onAddClick.bind(this)} headers={headers} data={this.state.item.clients||[]}/>
                    </div>
                    
            ) : (
                    <MuiThemeProvider>
                        <CircularProgress size={100} thickness={10} style={style}/>
                    </MuiThemeProvider>
                )
            }
            </div>

                


            
        )
    }
}